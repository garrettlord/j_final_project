NB. loads the data from the csv files into usable integer lists
NB. Author - George Schroeder
load'tables/csv' NB. for the readcsv call
require 'plot'

NB. loads the csv files into integer lists
NB. uses path given as the y argument
NB. example: "loaddata'/home/campus12/grschroe/Desktop/finalcs4121/j_final_project/'"
loaddata =: monad define
try.
]high_price=:;".&.>  readcsv jpath (y, 'high_price.csv')
]open_price=:;".&.> readcsv jpath (y, 'open_price.csv')
]low_price=:;".&.> readcsv jpath (y, 'low_price.csv')
]volume=:;".&.> readcsv jpath (y, 'volume.csv')
]closing_price=:;".&.> readcsv jpath (y, 'closing_price.csv')
]eom=: easeofmovement #high_price
]ef=: elderforce #high_price
'Successfully loaded the data'
catch.
print 'Error nonexisting or malformed data'
end.
)

graph =: monad define

NB. Graph
NB. DONE: Get # of days from file
x=: 1+i.#high_price NB. 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 NB. Days

keytxt=. 'High.,Low.,Open.,Closing.,Volume'
xlbl=. 'April 2014' NB. Todo: Get month from file
ylbl=. 'Price ($)'

NB. Graph init
pd 'reset'
pd 'xcaption ',xlbl
pd 'ycaption ',ylbl
pd 'xticpos ',":x
NB. TODO: Get yticpos from file

pd 'yticpos ', ": (((( ( (i.<./) high_price) { high_price) - ( ( (i.>./) high_price) { high_price)) % 10) * i. 10) + (((i.>./) high_price) { high_price)
NB. pd 'yticpos 170 172 174 176 178 180 182 184 186 188 190 192 194 196 198 200 202 204 206 208 210 212 214 216 218 220 222 224 226 228 230 232 234 236 238 240 242 244 246 248 250 252 254 256 258 260' NB. y intervals
pd 'ticstyle out' NB. tic lines outside of graph
pd 'grids 0 0'    NB. no gridlines
pd 'key ', keytxt
pd 'keycolor purple, green, red, cyan, magenta'
pd 'keystyle mho' NB. marker,horizontal,open
pd 'keypos tco'   NB. top,center,out
pd 'keymarkers line,line,line,line,line'


NB. 1st Function
pd 'type line' NB. type (line, bar, dot, stick, symbol)
pd 'color purple' NB. color (blue, green, red, purple, black, grey, yellow, cyan, magenta)
pd x;high_price NB. the data/function we are plotting


NB. 2nd Function
NB.pd 'type marker'
NB.pd 'markersize .2'
NB.pd 'markers circle'
pd 'type line'
pd 'color green'
pd x;low_price

pd 'type line'
pd 'color red'
pd x;open_price

pd 'type line'
pd 'color cyan'
pd x;closing_price

pd 'type line'
pd 'color magenta'
pd x;volume

NB. pd 'type line'
NB. pd 'color pink'
NB. pd x;ef

NB. pd 'type line'
NB. pd 'color yellow'
NB. pd x;eom

NB. pd 'canvas 600 800' NB. saves graph as html to ~temp/pdf (and opens it)
pd 'pdf' NB. saves graph as pdf to ~temp/pdf
NB. pd 'show' NB. shows the graph

)

NB. Author - George Schroeder
NB. this computes and returns y number of the elder force
NB. y must be smaller than the size of data
NB. the first list element is not valid and 0
elderforce =: monad define
y{.(;0; volume )*(;0; closing_price )+(; closing_price ;0)
)

NB. Author - George Schroeder
NB. this computes and returns y number of the ease of movement
NB. y must be smaller than the size of data
NB. the first list element is not valid and 0
easeofmovement =: monad define
]high_prev =. ; high_price ;0
]high_curr =. ;0; high_price
]low_prev =. ; low_price ;0
]low_curr =. ;0; low_price
]volume_curr =. ;0; volume 
y{. (((high_curr + low_curr) % 2) - ((high_prev + low_prev) % 2)) % ((volume_curr % 1000000) % (high_curr - low_curr))
)

loaddata'/Users/glord/code/progLang/j_final_project/csv/'
graph''
exit''


from __future__ import division
from itertools import islice, tee
import pandas
import pandas.io.data as web
import datetime
import urllib2
import csv
import ystockquote
import numpy as np
import csv
import sys


YAHOO_TODAY="http://download.finance.yahoo.com/d/quotes.csv?s=%s&f=sd1ohgl1vl1"

def get_quote_today(symbol):
    response = urllib2.urlopen(YAHOO_TODAY % symbol)
    reader = csv.reader(response, delimiter=",", quotechar='"')
    for row in reader:
        if row[0] == symbol:
            return row

def moving_average(n, iterable):
    # leading 0s
    for i in range(1, n):
        yield 0.

    # actual averages
    head, tail = tee(iterable)
    sum_ = float(sum(islice(head, n)))
    while True:
        yield sum_ / n
        sum_ += next(head) - next(tail)

def ExpMovingAverage(values, window):
    weights = np.exp(np.linspace(-1., 0., window))
    weights /= weights.sum()

    # Here, we will just allow the default since it is an EMA
    a =  np.convolve(values, weights)[:len(values)]
    a[:window] = a[window]
    return a #again, as a numpy array.

def movingaverage(values,window):
    weigths = np.repeat(1.0, window)/window
    smas = np.convolve(values, weigths, 'valid')
    return smas # as a numpy array

def EMV(d,c,h,l,o,v,tf):

    x=1
    OnepEMV = []
    while x < len(c):
        movement = ( ((h[x]+l[x])/2) - ((h[x-1]+l[x-1])/2) )
        boxr = ( (v[x]/1000000.00)/ (h[x]-l[x]) )
        OnepEMVs = movement / boxr
        OnepEMV.append(OnepEMVs)
        # print OnepEMVs
        x += 1
    tfEMV = movingaverage(OnepEMV,tf)
    # print len(tfEMV)
    # print len(d[tf:])

    return tfEMV

def EFI(d,c,v,tf): #Elder Force Index (EFI)
    efi = []

    x = 1
    while x < len(d):
        forceIndex = (c[x] - c[x-1]) * v[x]
        # print forceIndex
        efi.append(forceIndex)
        x+=1
    efitf = ExpMovingAverage(efi,tf)

    return d[1:], efitf

def CHMoF(d,c,h,l,o,v,tf):
    CHMF = []
    MFMs = []
    MFVs = []
    x = tf
    while x < len(d):
        PeriodVolume = 0
        volRange = v[x-tf:x]
        for eachVol in volRange:
            PeriodVolume += eachVol

        
        MFM = ((c[x]-l[x])-(h[x]-c[x]))/(h[x]-l[x])

        MFV = MFM*(PeriodVolume)

        MFMs.append(MFM)
        MFVs.append(MFV)
        x+=1

    y = tf
    while y < len(MFVs):
        PeriodVolume = 0
        volRange = v[y-tf:y]
        for eachVol in volRange:
            PeriodVolume += eachVol
            
        consider = MFVs[y-tf:y]
        #print consider
        tfsMFV = 0

        for eachMFV in consider:
            tfsMFV+=eachMFV

        tfsCMF = tfsMFV/PeriodVolume
        CHMF.append(tfsCMF)
        #print tfsCMF
        #time.sleep(555)
        y+=1
    #print len(CHMF)
    #print len(date[tf+tf:])
    return CHMF

## main ##
#print(sys.argv[1])
symbol = sys.argv[1]
# print symbol
history = pandas.io.data.DataReader(symbol, "yahoo", start="2014/1/1")
print history.tail(2)
today = datetime.date.today()
df = pandas.DataFrame(index=pandas.DatetimeIndex(start=today, end=today, freq="D"),
                      columns=["Open", "High", "Low", "Close", "Volume", "Adj Close"],
                      dtype=float)

row = get_quote_today(symbol)
df.ix[0] = map(float, row[2:])
history = history.append(df)

#Begining of grabbing indicators for file IO
start = datetime.datetime(2014, 02, 1)
end = datetime.datetime(2014, 04, 1)
f=web.DataReader(symbol, 'yahoo', start, end)

closing_price = f.Close
closing_length = closing_price
open_ = f.Open
high = f.High
low = f.Low
volume = f.Volume
easeOfMovement = EMV(closing_length,closing_price,high,low,open_,volume,14) #
Chaikin = CHMoF(closing_length,closing_price,high,low,open_,volume,14) #Chaikin Money Flow indicator
efix,efiy = EFI(closing_length,closing_price,volume,14)

# for index in range(0, len(open_)):
#     # print o
#     # print high[index]
#     print EMV(1,closeing_price[index],high[index],low[index],open_[index],volume[index],14)

f = open('/Users/glord/code/progLang/j_final_project/csv/closing_price.csv','w');
wr = csv.writer(f, quoting=csv.QUOTE_ALL)
wr.writerow(closing_price)
f.close()

f = open('/Users/glord/code/progLang/j_final_project/csv/open_price.csv','w');
wr = csv.writer(f, quoting=csv.QUOTE_ALL)
wr.writerow(open_)
f.close()

f = open('/Users/glord/code/progLang/j_final_project/csv/high_price.csv','w');
wr = csv.writer(f, quoting=csv.QUOTE_ALL)
wr.writerow(high)
f.close()

f = open('/Users/glord/code/progLang/j_final_project/csv/low_price.csv','w');
wr = csv.writer(f, quoting=csv.QUOTE_ALL)
wr.writerow(low)
f.close()


f = open('/Users/glord/code/progLang/j_final_project/csv/volume.csv','w');
wr = csv.writer(f, quoting=csv.QUOTE_ALL)
wr.writerow(low)
f.close()

f = open('/Users/glord/code/progLang/j_final_project/csv/moving_average.csv','w');
wr = csv.writer(f, quoting=csv.QUOTE_ALL)
wr.writerow((list(moving_average(10, closing_price))))
f.close()

# f = open('ExpMovingAverage_csv','w');
# f.write(ExpMovingAverage_csv)
# f.close()

f = open('/Users/glord/code/progLang/j_final_project/csv/easeOfMovement.csv','w');
wr = csv.writer(f, quoting=csv.QUOTE_ALL)
wr.writerow(easeOfMovement)
f.close()

f = open('/Users/glord/code/progLang/j_final_project/csv/chaikin.csv','w');
wr = csv.writer(f, quoting=csv.QUOTE_ALL)
wr.writerow(Chaikin)
f.close()

# moving_average = str(list(moving_average(10, closing_price)))
# ExpMovingAverage_csv = str(l)

# f = open('ExpMovingAverage.csv','w');
# wr = csv.writer(f, quoting=csv.QUOTE_ALL)
# wr.writerow(list(ExpMovingAverage_csv(closing_price, 20)))
# f.close()

# f = open('10_day_moving_average','w');
# f.write(moving_average)
# f.close()

#Coppock Curve = 10-period WMA of 14-period RoC + 11-perod RoC
#WMA = Weighted moving average
#RoC = Rate-of-Change
#ROC = [(Close - Close n periods ago) / (Close n periods ago)] * 100

#Will return a 3EMA of the dataset








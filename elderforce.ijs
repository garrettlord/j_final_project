NB. Author - George Schroeder
NB. this computes and returns y number of the elder force
NB. y must be smaller than the size of data
NB. the first list element is not valid and 0
elderforce =: monad define
y{.(;0; volume )*(;0; closing_price )+(; closing_price ;0)
)

NB. Author - George Schroeder
NB. this computes and returns y number of the ease of movement
NB. y must be smaller than the size of data
NB. the first list element is not valid and 0
easeofmovement =: monad define
]high_prev =. ; high_price ;0
]high_curr =. ;0; high_price
]low_prev =. ; low_price ;0
]low_curr =. ;0; low_price
]volume_curr =. ;0; volume 
y{. (((high_curr + low_curr) % 2) - ((high_prev + low_prev) % 2)) % ((volume_curr % 1000000) % (high_curr - low_curr))
)

import os.path

from flask import Flask, render_template, request
from forms import TickerInfo

  
app = Flask(__name__)
  
@app.route('/')
def home():
 return render_template('home.html')

# def home():
#   return render_template('home.html')

  
@app.route('/about')
def about():
  return render_template('about.html')

@app.route('/stockInfo', methods=['GET', 'POST'])
def stockInfo():
  form = TickerInfo()
 
  if request.method == 'POST':
    pythonString = "python ~/code/progLang/j_final_project/data_injest.py "
    pythonString = pythonString + request.form['ticker']
    print "------------------------------------"
    print pythonString
    os.system(pythonString)
    os.system("rm /Users/glord/j64-801-user/temp/plot.pdf")
    os.system("rm /Users/glord/code/progLang/j_final_project/flaskapp/app/static/plot.pdf")
    os.system("/Applications/j64-801/bin/jconsole /Users/glord/code/progLang/j_final_project/loaddata.ijs")
    os.system("cp /Users/glord/j64-801-user/temp/plot.pdf /Users/glord/code/progLang/j_final_project/flaskapp/app/static/")
    
    # print request.form['ticker']

 
    return render_template('stockInfo.html', form=form, success=True)
 
  elif request.method == 'GET':
    return render_template('stockInfo.html', form=form)

  
if __name__ == '__main__':
  app.run(debug=True)